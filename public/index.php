<?php
/**
 * 青岛华夏星火有限公司
 * @author 刘阳
 * @email yangakw@qq.com
 */
// 定义应用目录
define('APP_PATH', __DIR__ . '/administrator/app/');
// 加载框架引导文件
require __DIR__ . '/administrator/Core/start.php';
