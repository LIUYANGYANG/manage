<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 13:31
 */

namespace addons\payment;
use think\Addons;
use think\Db;

class Payment extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'message',	// 插件标识
        'title' => '账户信息管理',	// 插件名称
        'description' => '账户信息管理插件',	// 插件简介
        'status' => 0,	// 状态
        'author' => 'yangakw',
        'version' => '0.1'
    ];

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $sql = <<<EOF
        DROP TABLE IF EXISTS `payment`;
        CREATE TABLE `payment` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `memo` text COLLATE utf8_unicode_ci COMMENT '备注',
            `payment_type` int(11) NOT NULL COMMENT '支付类型',
            `code` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '交易码',
            `user_id` int(11) DEFAULT NULL COMMENT '用户',
            `value` int(11) DEFAULT NULL COMMENT '金额',
            `with_draw` int(11) DEFAULT NULL COMMENT '提现',
            `is_pay` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付成功',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
        PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOF;
        Db::query($sql);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        $sql = <<<EOF
        DROP TABLE IF EXISTS `payment`;
EOF;
        Db::query($sql);
        return true;
    }

    /**
     * 实现的 钩子方法
     * @return mixed
     */
    public function message_hook($param)
    {
        // 调用钩子时候的参数信息
        print_r($param);
        // 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
        print_r($this->getConfig());
        // 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }

}