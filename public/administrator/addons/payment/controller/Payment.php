<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/5
 * Time: 15:51
 */

namespace addons\payment\controller;


use think\addons\Controller;
use think\Db;

class Payment extends Controller
{
    const PAYED = 111;
    const PAY_FAILED = 100;
    const PAYING = 0;
    const IS_PAY = 1;

    /**
     * 审核提现成功
     * @throws \think\Exception
     */
    public function examine_success()
    {
        $p_id = input('p_id');
        Db::table('payment')->where(["id"=>$p_id])->update(['is_pay' => self::PAYED]);
        $this->success("success");
    }
    /**
     * 审核提现失败
     * @throws \think\Exception
     */
    public function examine_fail()
    {
        $p_id = input('p_id');
        Db::table('payment')->where(["id"=>$p_id])->update(['is_pay' => self::PAY_FAILED]);
        $this->success("success");
    }
}