<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 15:19
 */

namespace addons\payment\controller;


use think\addons\Controller;
use think\Db;
use think\Exception;

class Log extends Controller
{
    /**
     * 历史信息
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function payment_list()
    {
        Db::startTrans();
        try {
            $cond["with_draw"] = 0;
//            $cond = input();
            $data = Db::table("payment")->where($cond)->select();
            Db::commit();
        } catch (Exception $e) {
            $data = Db::table("payment")->select();
            Db::rollback();
        }
        $this->assign("payment_list", $data);
        return view("payment_list");

    }

    /**
     * 提现申请
     * @return \think\response\View
     */
    public function withdraw_list()
    {
        Db::startTrans();
        try {
            $cond["value"] = 0;
            $data = Db::table("payment")->where($cond)->select();
            Db::commit();
        } catch (Exception $e) {
            $data = Db::table("payment")->select();
            Db::rollback();
        }
        $this->assign("withdraw_list", $data);
        return view("withdraw_list");

    }

}