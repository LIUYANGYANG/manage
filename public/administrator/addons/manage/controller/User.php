<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 14:32
 */

namespace addons\manage\controller;


use addons\manage\Manage;
use app\common\Auth;
use think\addons\Controller;
use think\Db;

class User extends Controller
{
    /**
     * 设置用户为管理员
     */
    public function set_user_manage()
    {
        $uid = input('uid');
        Db::query(sprintf(
            "update `users` set `access` = %d where `id` = %d",
            Auth::ADMIN_ACCESS,
            $uid
        ));
        $this->success("success");
    }
    /**
     * 设置用户为管理员
     */
    public function cancel_user_manage()
    {
        $uid = input('uid');
        Db::query(sprintf(
            "update `users` set `access` = %d where `id` = %d",
            0,
            $uid
        ));
        $this->success("success");
    }

    /**
     * 关闭展览
     */
    public function close_expos()
    {
        $expos_id = input('expos_id');
        Db::query(sprintf(
            "update `expos` set `status` = %d  where `id` = %d",
            0,
            $expos_id
        ));
        $this->success("success");
    }

    /**
     * 关闭用户的展览
     */
    public function close_user_expos()
    {
        $uid = input('uid');
        Db::query(sprintf(
            "update `expos` set `status` = %d   where `user_id` = %d",
            0,
            $uid
        ));
        $this->success("success");
    }

    /**
     * 关闭用户
     */
    public function close_user()
    {
        $uid = input('uid');
        Db::query(sprintf(
            "update `users` set `password` = %d where `id` = %d",
            '',
            $uid
        ));
        $uid = input('uid');
        Db::query(sprintf(
            "update `expos` set `status` = %d  where `user_id` = %d",
            0,
            $uid
        ));
        $this->success("success");
    }
    /**
     * 关闭用户
     */
    public function reset_user()
    {
        $uid = input('uid');
        Db::query(sprintf(
            "update `users` set `password` = '%s' where `id` = %d",
            '3049a1f0f1c808cdaa4fbed0e01649b1',
            $uid
        ));

        $this->success("重置成功，默认密码 111");
    }
}