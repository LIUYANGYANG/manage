<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 13:31
 */

namespace addons\message;
use think\Addons;
use think\Db;

class Manage extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'message',	// 插件标识
        'title' => '管理员设置',	// 插件名称
        'description' => '管理员设置插件',	// 插件简介
        'status' => 0,	// 状态
        'author' => 'yangakw',
        'version' => '0.1'
    ];

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    /**
     * 实现的 钩子方法
     * @return mixed
     */
    public function message_hook($param)
    {
        // 调用钩子时候的参数信息
        print_r($param);
        // 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
        print_r($this->getConfig());
        // 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }

}