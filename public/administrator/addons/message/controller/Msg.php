<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 13:41
 */

namespace addons\message\controller;


use app\common\Auth;
use think\addons\Controller;
use think\Db;

class Msg extends Controller
{
    /**
     * 消息列表
     *
     */
    public function msg_list()
    {
        $me = Auth::get_loal_user();
        $to_id = $me->id;
        $msg_list = Db::table("message")->where(['to_user_id'=>$to_id])->order('id desc')->paginate(10);
        $this->assign("msg_list", $msg_list);
        return view("msg_list");
    }


    /**
     * 发送消息
     * 参数 to_id msg
     */
    public function send()
    {
        $to_id = input('to_id');
        $msg = input('msg');
        $from = Auth::get_loal_user();
        $from_id = $from->id;
        Db::table("message")->insert([
            "from_user_id"=>$from_id,
            "to_user_id"=>$to_id,
            "msg"=>$msg,
            "created_at"=>date("Y-m-d H:i:s"),
        ]);
        $this->success("success");
    }

    /**
     * 聊天
     * 参数 to_id
     * 返回html
     * @return \think\response\View
     */
    public function chat_msg(){
        $to_id = input('to_id');
        $from = Auth::get_loal_user();
        $from_id = $from->id;

        $condition['from_user_id'] = $from_id;
        $condition['from_user_id'] = $to_id;
        $condition['to_user_id'] = $from_id;
        $condition['to_user_id'] =$to_id;
        $l_d = Db::table("message")
            ->whereOr(function($query) use($from_id,$to_id){
                $query->where('from_user_id','=',$from_id)
                    ->where('to_user_id','=',$to_id);
            })
            ->whereOr(function($query) use($from_id,$to_id){
                $query->where('from_user_id','=',$to_id)
                    ->where('to_user_id','=',$from_id);
            })
            ->order("id desc")->select();
        $re_data = [];
        foreach($l_d as $k=>$v){
            $re_data[$v['id']]=$v;
        }
        ksort($re_data);

        $this->assign("me_id",$from_id);
        $this->assign("to_id",$to_id);
        $this->assign("list",$re_data);
        return view("chat_msg");

    }
    /**
     * 聊天
     * 参数 to_id
     * 返回html
     * @return \think\response\View
     */
    public function chat(){
        $to_id = input('to_id');
        $from = Auth::get_loal_user();
        $from_id = $from->id;
        $this->assign("me_id",$from_id);
        $this->assign("to_id",$to_id);
        return view("chat");

    }
}