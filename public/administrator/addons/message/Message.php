<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 13:31
 */

namespace addons\message;
use think\Addons;
use think\Db;

class Message extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'message',	// 插件标识
        'title' => '消息插件',	// 插件名称
        'description' => '消息插件第一个插件',	// 插件简介
        'status' => 0,	// 状态
        'author' => 'yangakw',
        'version' => '0.1'
    ];

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $sql = <<<EOF
        DROP TABLE IF EXISTS `message`;
        CREATE TABLE `message` (
            `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
          `msg` text COLLATE utf8_unicode_ci COMMENT '内容',
          `from_user_id` int(11) DEFAULT NULL COMMENT '来自谁',
          `to_user_id` int(11) DEFAULT NULL COMMENT '发给谁',
          `is_read` tinyint(4) DEFAULT '0' COMMENT '已读',
          `created_at` timestamp NULL DEFAULT NULL,
          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOF;
        Db::query($sql);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        $sql = <<<EOF
        DROP TABLE IF EXISTS `message`;
EOF;
        Db::query($sql);
        return true;
    }

    /**
     * 实现的 钩子方法
     * @return mixed
     */
    public function message_hook($param)
    {
        // 调用钩子时候的参数信息
        print_r($param);
        // 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
        print_r($this->getConfig());
        // 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }

}