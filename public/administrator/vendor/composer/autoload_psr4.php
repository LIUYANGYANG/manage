<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'think\\worker\\' => array($vendorDir . '/topthink/think-worker/src'),
    'think\\migration\\' => array($vendorDir . '/topthink/think-migration/src'),
    'think\\helper\\' => array($vendorDir . '/topthink/think-helper/src'),
    'think\\composer\\' => array($vendorDir . '/topthink/think-installer/src'),
    'think\\captcha\\' => array($vendorDir . '/topthink/think-captcha/src'),
    'think\\angular\\' => array($vendorDir . '/topthink/think-angular/src'),
    'think\\' => array($baseDir . '/Core/library/think', $vendorDir . '/topthink/think-image/src', $vendorDir . '/topthink/think-queue/src', $vendorDir . '/5ini99/think-addons/src'),
    'Workerman\\' => array($vendorDir . '/workerman/workerman'),
    'Phinx\\' => array($vendorDir . '/topthink/think-migration/phinx/src/Phinx'),
);
