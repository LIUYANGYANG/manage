<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/30
 * Time: 18:00
 */
return [
    // 定义index模块的自动生成
    'common'    => [
        '__file__'   => ['common.php'],
        '__dir__'    => [ 'model'],
        'model'      => [
            "Beenwatches",
            "Educations",
            "Emails",
            "Expodetails",
            "Expoitems",
            "Expos",
            "Languages",
            "Migrations",
            "Professionals",
            "Skills",
            "Telephones",
            "Watches"
        ],
    ],
    // 。。。 其他更多的模块定义
];