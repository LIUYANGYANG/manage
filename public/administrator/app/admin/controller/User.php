<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/10/28 0028
 * Time: 上午 10:15
 */

namespace app\admin\controller;


use app\common\controller\Admin;
use app\common\model\Expos;
use app\common\model\Users;

class User extends Admin
{
    public function __construct(\think\Request $request)
    {
        parent::__construct($request);
        \think\Hook::listen('auth_begin');
    }

    public function works_examine()
    {
        return view();
    }

    public function user_list()
    {
        $param = input();
        $user_list = Users::getUserList($param);
        $this->assign("user_list", $user_list);
        return view();
    }

}
