<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/10/28 0028
 * Time: 上午 10:20
 */

namespace app\admin\controller;


use app\common\Auth;
use app\common\controller\Admin;
use app\common\model\Expodetails;
use app\common\model\Expoitems;
use app\common\model\Expos;

class Exposition extends Admin
{
    public function __construct(\think\Request $request)
    {
        parent::__construct($request);
        \think\Hook::listen('auth_begin');
    }

    /**
     * @return \think\response\View
     */
    public function banner()
    {
        $param = input();
        $banner_list = \app\common\model\Banner::all_banner($param);
        $banner_cate_list = \app\common\model\BannerCate::all();
        $this->assign("banner_list", $banner_list);
        $this->assign("banner_cate_list", $banner_cate_list);
        return view();
    }

    /**
     * @return \think\response\View
     */
    public function edit_banner()
    {
        $banner = \app\common\model\Banner::get(input("id"));
        if (input('delete')) {
            $banner->delete();
            $this->success("success");
        } else if (
            input('image')
            && input('desc')
            && input('cate_id')
            && input('url')
            && input('submit')
        ) {
            if (empty($banner)) {
                $banner = new  \app\common\model\Banner();
            }
            $banner->image = input('image');
            $banner->desc = input('desc');
            $banner->url = input('url');
            $banner->cate_id = input('cate_id');
            $banner->save();
            $this->success("success");
        } else if (
        input('submit')
        ) {
            $this->error("param error");
        }
        $banner_cate_list = \app\common\model\BannerCate::all();
        $this->assign("banner_cate_list", $banner_cate_list);
        $this->assign("banner", $banner);
        return view();
    }

    /**
     * @return \think\response\View
     */
    public function edit_banner_cate()
    {
        $banner_cate = \app\common\model\BannerCate::get(input('id'));
        if (input('delete')) {
            $banner_cate->delete();
            $this->success("success");
        } else if (
            input('title')
            && input('submit')
            && input('code')
        ) {
            if (empty($banner_cate)) {
                $banner_cate = new  \app\common\model\BannerCate();
            }
            $banner_cate->title = input('title');
            $banner_cate->code = input('code');
            $banner_cate->save();
            $this->success("success");
        } else if (
        input('submit')
        ) {
            $this->error("param error");
        }
        $this->assign("banner_cate", $banner_cate);
        return view();
    }

    /**
     * @param int $id
     * @return int|\think\response\View
     */
    public function edit_expos($id = 0)
    {
        /** @var Expos $expos */
        $expos = Expos::get($id);
        if (input("submit")) {
            if (empty($expos)) {
                $expos = new Expos();
            }
            $expos->title = input("title");
            $expos->img_cover = input("img_cover");//封面
            $expos->type = input("type");
            $expos->starttime = input("starttime");
            $expos->endtime = input("endtime");
            $expos->cost = input("cost");
            $expos->taglist = input("taglist");
            $expos->subject = input("subject");
            $expos->abstract = input("abstract");
            $expos->user_id = Auth::$user->id;
            $expos->status = 1;
            $expos->save();
            $this->success("success");
        } else if (input("delete")) {
            if (!empty($expos)) {
                $expos->delete();
                $this->success("success");
            }
        } else {
            $this->assign("expos", $expos);
            return view();
        }
        return 0;
    }

    /**
     * @param int $id
     * @return \think\response\View
     */
    public function edit_expos_details($id = 0)
    {
        /** @var Expos $expos */
        $expos = Expos::get($id);
        if (empty($expos)) {
            $this->error("expos not exists");
        }
        $expos_details = $expos->getExpodetails();

        $this->assign("expos", $expos);
        $this->assign("expos_details", $expos_details);
        return view();
    }

    /**
     * @param int $expos_id
     * @param int $id
     * @return \think\response\View
     */
    public function edit_expos_detail($expos_id = 0, $id = 0)
    {
        /** @var Expos $expos */
        $expos = Expos::get($expos_id);
        if (empty($expos))
            $this->error("expos not exists");
        $Expodetails = Expodetails::get($id);
        if (input('submit')) {
            if (empty($Expodetails)) {
                $Expodetails = new Expodetails();
            }
            $Expodetails->image = input('image');
            $Expodetails->title = input('title');
            $Expodetails->abstract = input('abstract');
            $Expodetails->expo_id = $expos->id;
            $Expodetails->save();
            $this->success("success");
        } else if (input('delete')) {
            if (!empty($Expodetails)) {
                $Expodetails->delete();
            }
            $this->success("success");
        }
        $this->assign("Expodetails", $Expodetails);
        $this->assign("expos", $expos);
        return view();
    }

    /**
     * @param int $id
     * @return \think\response\View
     */

    public function edit_expos_items($id = 0)
    {
        /** @var Expodetails $expos_detail */
        $expos_detail = Expodetails::get($id);
        if (empty($expos_detail)) {
            $this->error("Expodetails not exists");
        }
        $expos_items = $expos_detail->getExpoitems();

        $this->assign("expos_detail", $expos_detail);
        $this->assign("expos_items", $expos_items);
        return view();
    }

    /**
     * @param int $expos_detail_id
     * @param int $id
     * @return \think\response\View
     */
    public function edit_expos_item($expos_detail_id = 0, $id = 0)
    {
        /** @var Expodetails $expos_details */
        $expos_details = Expodetails::get($expos_detail_id);
        if (empty($expos_details))
            $this->error("Expodetails not exists");
        /** @var Expoitems $Expoitems */
        $Expoitems = Expoitems::get($id);
        if (input('submit')) {
            if (empty($Expoitems)) {
                $Expoitems = new Expoitems();
            }
            $Expoitems->image = input('image');
            $Expoitems->title = input('title');
            $Expoitems->abstract = input('abstract');
            $Expoitems->expodetail_id = $expos_details->id;
            $Expoitems->save();
            $this->success("success");
        } else if (input('delete')) {
            if (!empty($Expoitems)) {
                $Expoitems->delete();
            }
            $this->success("success");
        }
        $this->assign("expoitem", $Expoitems);
        $this->assign("expos_details", $expos_details);
        return view();
    }

    /**
     * @return \think\response\View
     */
    public function official()
    {
        $expos = Expos::all(["type" => "official"]);
        $this->assign("type", "official");
        $this->assign("expos", $expos);
        return view("expos");
    }

    /**
     * @return \think\response\View
     */
    public function personal()
    {
        $expos = Expos::all(["type" => "personal"]);
        $this->assign("type", "personal");
        $this->assign("expos", $expos);
        return view("expos");
    }

    /**
     * @return \think\response\View
     */
    public function school()
    {
        $expos = Expos::all(["type" => "school"]);
        $this->assign("type", "school");
        $this->assign("expos", $expos);
        return view("expos");
    }

    /**
     * @return \think\response\View
     */
    public function recommend()
    {
        $expos = Expos::all(["type" => "recommend"]);
        $this->assign("type", "recommend");
        $this->assign("expos", $expos);
        return view("expos");
    }

    /**
     * @return \think\response\View
     */
    public function other()
    {
        $expos = Expos::all(["type" => "other"]);
        $this->assign("type", "other");
        $this->assign("expos", $expos);
        return view("expos");
    }


    /**
     * 待审核展览
     * @return \think\response\View
     */
    public function user_expos()
    {
        $this->assign("type", "examining");
        $cond["status"] = 0;
        $list = Expos::all($cond);
        $this->assign("expos", $list);
        return view("expos");
    }

    /**
     * 审核操作
     * @param $id
     * @param $status
     */
    public function examine_expos($id, $status)
    {
        $expos = Expos::get($id);
        if(!$expos){
            $this->error("not found expos");
        }
        $expos->status = $status == 1 ? 1 : 0;
        $expos->save();
        $this->success("success");
    }

    /**
     * @return string|\think\response\View
     */
    public function preview()
    {
        if (input("expos_id")) {
            /** @var Expos $expos */
            $expos = Expos::get(input("expos_id"));
            $this->assign("expos", $expos);
            $this->assign("expos_details", $expos->getExpodetails());
            return view("preview_expos");
        } else if (input("expos_detail_id")) {
            /** @var Expodetails $expodetails */
            $expodetails = Expodetails::get(input("expos_detail_id"));
            $this->assign("expos_items", $expodetails->getExpoitems());
            $this->assign("expos_detail", $expodetails);
            return view("preview_expos_detail");
        }
        return "";
    }

    /**
     * @param string $src
     * @return \think\response\View
     */
    public function show_image($src = '')
    {
        if (empty($src)) {
            $this->error("image not exist!!!");
        }
        $this->assign('src', $src);
        return view();
    }
}