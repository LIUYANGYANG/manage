<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/30
 * Time: 11:22
 */

namespace app\admin\controller;


use app\common\controller\Admin;

class Api extends Admin
{
    /**
     * 测试文档
     * @param string $domain
     * @return array|mixed|\think\response\View
     */
    public function api_doc($domain = "")
    {
        $config = [
            "\\app\\admin\\controller\\Tool",
            "\\addons\\manage\\controller\\User",
            "\\addons\\message\\controller\\Msg",
            "\\addons\\payment\\controller\\Log",
            "\\addons\\payment\\controller\\Payment",
        ];
        $r_data = [];
        if (!is_array($config)) {
            exit("config error");
        }
        foreach ($config as $k => $class) {
            $ref = new \ReflectionClass($class);
            $methods = $ref->getMethods();
            if (!is_array($methods)) {
                continue;
            }
            foreach ($methods as $method) {
                $class = $method->class;
                $action = $method->name;
                $param = $method->getParameters();
                $doc = $method->getDocComment();
                if (strpos($class, "controller") != false) {
                    $c = str_replace("\\", "/", $class);
                    $c = str_replace("/controller/", "/", $c);
                    $c = str_replace("app/", "", $c);
                    if(strpos($c,'ddons')){
                        $c = str_replace("addons/", "", $c);
                        $c = str_replace("/", "-", $c);
                        $url = sprintf("/addons/execute/%s-%s", $c, $action);
                        $i_data["url"] = sprintf("<a href='%s'>%s</a>",$url,$url);
                    }else{
                        $i_data["url"] = sprintf("%s/%s/%s", $domain, $c, $action);
                    }

                    $i_data["param"] = $param;
                    $doc = str_replace("/*", "", $doc);
                    $doc = str_replace("*/", "", $doc);
                    $doc = str_replace("*", "", $doc);
                    $i_data["doc"] = str_replace("\n", "<br/>", $doc);
                    $i_data["action"] = $class . "->" . $action . "()";
                    $i_data["class"] = $class ;
                    $r_data[] = $i_data;
                }
            }
        }
        $this->assign("r_data", $r_data);
        return view();
    }
}