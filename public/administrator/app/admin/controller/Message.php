<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/10/28 0028
 * Time: 上午 10:18
 */

namespace app\admin\controller;


use app\common\controller\Admin;

class Message extends Admin
{
    public function __construct(\think\Request $request)
    {
        parent::__construct($request);
        \think\Hook::listen('auth_begin');
    }
    public function withdrawal_request(){
        return view();
    }
    public function history(){
        return view();
    }
    public function user_question(){
        return view();
    }
}