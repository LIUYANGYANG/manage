<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/30
 * Time: 14:03
 */

namespace app\common;


use app\common\model\Users;
use think\Controller;

class Auth extends Controller
{
    const ADMIN_ACCESS = 111;
    public static $user;

    public static function get_loal_user()
    {
        if (empty(self::$user)) {
            $user_id = session("user_id");
            $user = Users::getUser($user_id);
            #管理员权限
            if(isset($user->access) && $user->access == self::ADMIN_ACCESS){
                self::$user = $user;
            }
        }
        return self::$user;
    }

    public static function login(Users $user)
    {
        return session("user_id", $user->id);
    }

    public function run()
    {
        if (!(
        self::get_loal_user()
        )
        ) {
            $this->error("please login in", "/admin/index/login");
        }
    }
}