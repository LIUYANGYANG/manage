<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/30
 * Time: 11:16
 */

namespace app\common;


class Upload
{
    const UPLOAD_PATH = "uploads";

    /**
     * 单个文件上传 input name="file"
     *
     */
    public static function save_file(){
        do{
            // 获取表单上传文件 例如上传了001.jpg
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            if($file){
                $info = $file->move(ROOT_PATH . '../'. self::UPLOAD_PATH);
                if($info){
                    // 成功上传后 获取上传信息
                    $file_path = self::UPLOAD_PATH."/".$info->getSaveName();
                    $path = sprintf("/%s",$file_path);
                    $path = str_replace("\\","/",$path);
                    return $path;
                }else{
                    // 上传失败获取错误信息
                    return false;
                }
            }else{
                return false;
            }
        }while(0);
    }
}