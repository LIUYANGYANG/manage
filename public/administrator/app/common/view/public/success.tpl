{__NOLAYOUT__}
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<style>
	#toast{
		position: fixed;
		top: 20px;
		left: 40%;
		width:300px;
		border-radius: 15px;
		font-size: 24px;;
		margin-left: -100px;
		border: 1px solid #CCC;
		background-color: #111;
		color:#FFF;
		padding: 10px 0 ;
		text-align:center;
	}
</style>
<div id="toast">
	<?php echo(strip_tags($msg));?>
	</div>
<script>

	setTimeout(function(){
		location.href='<?php echo($url);?>';
	},100)
</script>