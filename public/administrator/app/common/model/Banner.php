<?php
/**
 * Created by phpstorem.
 * User: yangakw@qq.com
 * Sign: 猥琐发育，别浪
 * Date: 2017/11/5 0005
 * Time: 下午 8:35
 */

namespace app\common\model;


use think\Model;

class Banner extends Model
{
    public static function all_banner($param){
        $cond=[];
        if(isset($param["cate_id"]) && $param["cate_id"]>0){
            $cond["cate_id"]=$param["cate_id"];
        }
        return self::all(function($query) use ($cond){
            $query
                ->field("banner.*,banner_cate.title")
                ->join(
                "banner_cate","banner_cate.id=banner.cate_id"
            )->where($cond);
        });
    }
}