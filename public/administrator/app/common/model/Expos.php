<?php
namespace app\common\model;

use think\Model;

class Expos extends Model
{
    private function is_null()
    {
        if (empty($this)) {
            exit("empty Expos!!!");
        }
    }

    /**
     * 获取detail
     * @return false|static[]
     */
    public function getExpodetails()
    {
        $this->is_null();
        $r_list = Expodetails::all(["expo_id" => $this->id]);
        return $r_list;
    }

    /**
     * 喜欢
     * @return int|true
     */
    public function likeExpos()
    {
        $this->is_null();
        return $this->setInc('score');
    }
}