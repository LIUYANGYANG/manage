<?php
namespace app\common\model;

use think\Model;

class Users extends Model
{
    public static function getUserList($param){
        $cond=[];
        return self::all(function($query) use ($cond){
            $query->where($cond);
        });
    }
    public static function getUser($param)
    {
        if (is_numeric($param)) {
            $me = self::get(['id' => $param]);
        } else if (is_array($param)) {
            $me = self::get($param);
        } else if ($param instanceof \Closure) {
            $me = new self();
            $me = $me->find($param);
        } else {
            $me = null;
        }
        return $me;
    }

    /**
     * 教育
     * @return null|static
     */
    public function getEducation()
    {
        $edu = Educations::all(["user_id" => $this->id]);
        return $edu;
    }

    /**
     * 邮箱
     * @return null|static
     */
    public function getEmail()
    {
        $email = Emails::all(["user_id" => $this->id]);
        return $email;
    }

    /**
     *展览评论
     * @return null|static
     */
    public function getExpocomments()
    {
        $Expocomments = Expocomments::all(["user_id" => $this->id]);
        return $Expocomments;
    }

    /**
     * 展览详情
     * @return null|static
     */
    public function getExpodetails()
    {
        $Expodetails = Expodetails::all(["user_id" => $this->id]);
        return $Expodetails;
    }

    /**
     * 展览单品
     * @return null|static
     */
    public function getExpoitems()
    {
        $item = Expoitems::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     * 展览项目
     * @return null|static
     */
    public function getExpos()
    {
        $item = Expos::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     * 语言
     * @return null|static
     */
    public function getLanguages()
    {
        $item = Languages::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     *专业
     * @return null|static
     */
    public function getProfessionals()
    {
        $item = Professionals::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     * 技能
     * @return null|static
     */
    public function getSkills()
    {
        $item = Skills::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     * 电话
     * @return null|static
     */
    public function getTelephones()
    {
        $item = Telephones::all(["user_id" => $this->id]);
        return $item;
    }

    /**
     * 访问量
     * @return null|static
     */
    public function getWatches()
    {
        $item = Watches::all(["user_id" => $this->id]);
        return $item;
    }
}