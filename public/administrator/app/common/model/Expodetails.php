<?php
namespace app\common\model;

use think\Model;

class Expodetails extends Model
{
    public  function getExpoitems()
    {
        $items = Expoitems::all(["expodetail_id" => $this->id]);
        return $items;
    }
}