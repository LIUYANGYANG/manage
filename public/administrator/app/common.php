<?php

use think\Log;

/**
 * 用户信息
 * @param $user_id
 * @param $cb
 * @return mixed
 */
function user_info($user_id, $cb)
{
    $r_data = \app\common\model\Users::get($user_id);
    return $cb($r_data);
}