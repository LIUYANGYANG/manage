<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/4
 * Time: 13:28
 */
return [
    // 可以定义多个钩子
    'message_hook'=>'message' // 键为钩子名称，用于在业务中自定义钩子处理，值为实现该钩子的插件，
    // 多个插件可以用数组也可以用逗号分割
];