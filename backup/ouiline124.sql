/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : ouiline

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2017-12-04 17:37:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `banner`
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `memo` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('1', null, '111', '111', null, null);
INSERT INTO `banner` VALUES ('2', null, '913', '111', null, null);
INSERT INTO `banner` VALUES ('3', null, '14009', '111', null, null);
INSERT INTO `banner` VALUES ('4', null, '/uploads/20171106/55b11b2358e285899d2d9c0fc09d0677.png', '主页轮播', null, null);

-- ----------------------------
-- Table structure for `banner_cate`
-- ----------------------------
DROP TABLE IF EXISTS `banner_cate`;
CREATE TABLE `banner_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memo` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of banner_cate
-- ----------------------------

-- ----------------------------
-- Table structure for `beenwatches`
-- ----------------------------
DROP TABLE IF EXISTS `beenwatches`;
CREATE TABLE `beenwatches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beenwatch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `beenwatches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of beenwatches
-- ----------------------------

-- ----------------------------
-- Table structure for `educations`
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `education` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `educations_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of educations
-- ----------------------------

-- ----------------------------
-- Table structure for `emails`
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emails_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of emails
-- ----------------------------

-- ----------------------------
-- Table structure for `expocomments`
-- ----------------------------
DROP TABLE IF EXISTS `expocomments`;
CREATE TABLE `expocomments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expocomments_user_id_foreign` (`user_id`),
  KEY `expocomments_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expocomments
-- ----------------------------

-- ----------------------------
-- Table structure for `expodetails`
-- ----------------------------
DROP TABLE IF EXISTS `expodetails`;
CREATE TABLE `expodetails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expodetails_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expodetails
-- ----------------------------
INSERT INTO `expodetails` VALUES ('2', '123', '21212', '/uploads/20171107/4116fe97bfa26c4c6659e60b6cdd7f92.jpg', '3');
INSERT INTO `expodetails` VALUES ('4', '1111', '1111', '/uploads/20171107/46b1978410b5a47d59a84689aff84038.jpg', '3');
INSERT INTO `expodetails` VALUES ('5', 'school', 'school', '/uploads/20171107/6506a8e55855b370c81e3dba3fd2c7c7.jpg', '7');
INSERT INTO `expodetails` VALUES ('6', 'school', 'school', '/uploads/20171107/95b4484c409f899764f363f7581985f4.jpg', '8');
INSERT INTO `expodetails` VALUES ('7', '23', '12', '/uploads/20171107/ea49a65d5c539f3687e905bbbf8d1e00.png', '10');
INSERT INTO `expodetails` VALUES ('8', '23324', '22423', '/uploads/20171108/f2638af2d16165c2fc637194c249a97d.jpg', '3');
INSERT INTO `expodetails` VALUES ('9', 'cudst', 'cudst', '/uploads/20171108/a2977b91d21936788932ff89d2b4c744.png', '9');

-- ----------------------------
-- Table structure for `expoitems`
-- ----------------------------
DROP TABLE IF EXISTS `expoitems`;
CREATE TABLE `expoitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expodetail_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expoitems_expodetail_id_foreign` (`expodetail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expoitems
-- ----------------------------
INSERT INTO `expoitems` VALUES ('1', '', '', '/uploads/20171113/1bd5a00af43bd7d446dc9995be41836a.jpg', '6');
INSERT INTO `expoitems` VALUES ('2', '', '', '/uploads/20171113/4cfcaa04d39dde63e3b5105cca592fb6.jpg', '6');
INSERT INTO `expoitems` VALUES ('3', '', '', '/uploads/20171113/7803ac20c577c436690f9a1ad596d482.png', '2');
INSERT INTO `expoitems` VALUES ('4', '', '', '/uploads/20171113/9fbde03d1502d8dc1f7614223e3deda7.jpg', '2');
INSERT INTO `expoitems` VALUES ('5', '', '', '/uploads/20171113/ba34f7b0a6cfb3431c5c752c622bfa89.jpg', '2');
INSERT INTO `expoitems` VALUES ('6', '', '', '/uploads/20171113/c84b482387fa3539806b26df690a5ee2.jpg', '2');
INSERT INTO `expoitems` VALUES ('7', '', '', '/uploads/20171113/a784290694321d479fe8a351eb5f0c78.jpg', '2');
INSERT INTO `expoitems` VALUES ('8', '', '', '/uploads/20171113/5202fc25cdcb660c37aec73e19c4c893.jpg', '2');
INSERT INTO `expoitems` VALUES ('9', '', '', '/uploads/20171113/461315aebbb7e137dc749c661011dbed.jpg', '2');
INSERT INTO `expoitems` VALUES ('10', '', '', '/uploads/20171113/2b61e68b73a4c09bb816e9cf5b98a891.png', '4');
INSERT INTO `expoitems` VALUES ('11', '', '', '/uploads/20171113/a3943c27a8ff797dd0682b6fd9f82c18.jpg', '4');
INSERT INTO `expoitems` VALUES ('12', '', '', '/uploads/20171113/a694434f16e21924818142c9116af29f.jpg', '4');
INSERT INTO `expoitems` VALUES ('13', '', '', '/uploads/20171113/66ea09b92ace92a3abdd46721b348806.jpg', '4');
INSERT INTO `expoitems` VALUES ('14', '', '', '/uploads/20171113/e076609a125e6def6aa85fe737d441fd.jpg', '4');
INSERT INTO `expoitems` VALUES ('15', '', '', '/uploads/20171113/0cd3b2a98f734a4d4661233b54736712.jpg', '8');
INSERT INTO `expoitems` VALUES ('16', '', '', '/uploads/20171113/99382da5d0688d3ec50c2914d6b771e2.jpg', '8');
INSERT INTO `expoitems` VALUES ('17', '', '', '/uploads/20171113/dfa2caf6cbfb1229d8297916d4f2c67c.jpg', '8');
INSERT INTO `expoitems` VALUES ('18', '', '', '/uploads/20171113/e6f6b4bbbdda10c1672ead9cac3d765c.jpg', '8');

-- ----------------------------
-- Table structure for `expos`
-- ----------------------------
DROP TABLE IF EXISTS `expos`;
CREATE TABLE `expos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starttime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost` double(8,2) DEFAULT NULL,
  `taglist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `followcount` int(10) unsigned NOT NULL DEFAULT '0',
  `likecount` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `status` int(11) DEFAULT '0' COMMENT '审核状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expos_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expos
-- ----------------------------
INSERT INTO `expos` VALUES ('3', 'title', '/uploads/20171204/c9835dabc8ce434dc082bd2028e1255a.jpg', 'official', '0', '0', '0.00', 'sadas', 'rows', '2121<img src=\"/uploads/20171107/0b8cdadbe0adcc0f078794c2d40a4e64.png\" alt=\"undefined\">', '0', '0', '0', '0', null, null);
INSERT INTO `expos` VALUES ('7', '个人', null, 'personal', '0', '0', '1.00', '1', '个人', '<p>个人</p>', '0', '0', '0', '1', null, null);
INSERT INTO `expos` VALUES ('8', 'school', null, 'school', '0', '0', '1.00', 'school', 'school', '<p>school<img src=\"/uploads/20171107/23910e447a72534e4ea85c09fa70bf8c.jpg\" alt=\"undefined\"></p>', '0', '0', '0', '1', null, null);
INSERT INTO `expos` VALUES ('11', 'cf', '/uploads/20171204/3899fa53cacf0cfaa062414e3068f8b6.jpg', 'personal', '2017-12-04 17:30:09', '2017-12-04 17:30:10', '12.00', '21', 'cols', '221', '0', '0', '4', '1', null, null);

-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `languages_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `msg` text COLLATE utf8_unicode_ci COMMENT '内容',
  `from_user_id` int(11) DEFAULT NULL COMMENT '来自谁',
  `to_user_id` int(11) DEFAULT NULL COMMENT '发给谁',
  `is_read` tinyint(4) DEFAULT '0' COMMENT '已读',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2017_07_23_073250_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2017_07_23_073329_create_articles_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_07_23_073447_create_articlecomments_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_07_23_073502_create_articledetails_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_07_23_073526_create_educations_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_07_23_073545_create_emails_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_07_23_073557_create_expos_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_07_23_073614_create_expocomments_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_07_23_073627_create_expodetails_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_07_23_073700_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_07_23_073715_create_professionals_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_07_23_073729_create_skills_table', '1');
INSERT INTO `migrations` VALUES ('13', '2017_07_23_073744_create_telephones_table', '1');
INSERT INTO `migrations` VALUES ('14', '2017_07_23_073804_create_watches_table', '1');
INSERT INTO `migrations` VALUES ('15', '2017_07_23_073835_create_watchedes_table', '1');
INSERT INTO `migrations` VALUES ('16', '2017_08_08_063014_create_jobInfos_table', '1');
INSERT INTO `migrations` VALUES ('17', '2017_08_08_080646_create_expoItems_table', '1');

-- ----------------------------
-- Table structure for `payment`
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memo` text COLLATE utf8_unicode_ci COMMENT '备注',
  `payment_type` int(11) NOT NULL COMMENT '支付类型',
  `code` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '交易码',
  `value` int(11) DEFAULT '0' COMMENT '金额',
  `user_id` int(11) DEFAULT NULL,
  `with_draw` int(11) DEFAULT '0' COMMENT '提现',
  `is_pay` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付成功',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES ('1', null, '0', '', '1', null, '0', '0', '2017-12-04 16:55:06');
INSERT INTO `payment` VALUES ('2', null, '0', '', '0', null, '1', '0', '2017-12-04 16:55:08');

-- ----------------------------
-- Table structure for `professionals`
-- ----------------------------
DROP TABLE IF EXISTS `professionals`;
CREATE TABLE `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `professionals_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of professionals
-- ----------------------------

-- ----------------------------
-- Table structure for `skills`
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of skills
-- ----------------------------

-- ----------------------------
-- Table structure for `telephones`
-- ----------------------------
DROP TABLE IF EXISTS `telephones`;
CREATE TABLE `telephones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `telephones_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of telephones
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `givenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephonevis` tinyint(4) NOT NULL DEFAULT '0',
  `emailvis` tinyint(4) NOT NULL DEFAULT '0',
  `wechatvis` tinyint(4) NOT NULL DEFAULT '0',
  `facebooksvis` tinyint(4) NOT NULL DEFAULT '0',
  `educationvis` tinyint(4) NOT NULL DEFAULT '0',
  `professionalvis` tinyint(4) NOT NULL DEFAULT '0',
  `skillvis` tinyint(4) NOT NULL DEFAULT '0',
  `access` tinyint(4) NOT NULL DEFAULT '0',
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', '111', '3049', '111', '111', null, '111', null, null, '111', '0', '0', '0', '0', '0', '0', '111', null, null, null);

-- ----------------------------
-- Table structure for `watches`
-- ----------------------------
DROP TABLE IF EXISTS `watches`;
CREATE TABLE `watches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `watch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `watches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of watches
-- ----------------------------
