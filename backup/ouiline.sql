/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : ouiline

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2017-10-30 13:53:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `beenwatches`
-- ----------------------------
DROP TABLE IF EXISTS `beenwatches`;
CREATE TABLE `beenwatches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beenwatch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `beenwatches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of beenwatches
-- ----------------------------

-- ----------------------------
-- Table structure for `educations`
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `education` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `educations_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of educations
-- ----------------------------

-- ----------------------------
-- Table structure for `emails`
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emails_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of emails
-- ----------------------------

-- ----------------------------
-- Table structure for `expocomments`
-- ----------------------------
DROP TABLE IF EXISTS `expocomments`;
CREATE TABLE `expocomments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expocomments_user_id_foreign` (`user_id`),
  KEY `expocomments_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expocomments
-- ----------------------------

-- ----------------------------
-- Table structure for `expodetails`
-- ----------------------------
DROP TABLE IF EXISTS `expodetails`;
CREATE TABLE `expodetails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expodetails_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expodetails
-- ----------------------------

-- ----------------------------
-- Table structure for `expoitems`
-- ----------------------------
DROP TABLE IF EXISTS `expoitems`;
CREATE TABLE `expoitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expodetail_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expoitems_expodetail_id_foreign` (`expodetail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expoitems
-- ----------------------------

-- ----------------------------
-- Table structure for `expos`
-- ----------------------------
DROP TABLE IF EXISTS `expos`;
CREATE TABLE `expos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starttime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost` double(8,2) DEFAULT NULL,
  `taglist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `followcount` int(10) unsigned NOT NULL DEFAULT '0',
  `likecount` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expos_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of expos
-- ----------------------------

-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `languages_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2017_07_23_073250_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2017_07_23_073329_create_articles_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_07_23_073447_create_articlecomments_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_07_23_073502_create_articledetails_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_07_23_073526_create_educations_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_07_23_073545_create_emails_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_07_23_073557_create_expos_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_07_23_073614_create_expocomments_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_07_23_073627_create_expodetails_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_07_23_073700_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_07_23_073715_create_professionals_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_07_23_073729_create_skills_table', '1');
INSERT INTO `migrations` VALUES ('13', '2017_07_23_073744_create_telephones_table', '1');
INSERT INTO `migrations` VALUES ('14', '2017_07_23_073804_create_watches_table', '1');
INSERT INTO `migrations` VALUES ('15', '2017_07_23_073835_create_watchedes_table', '1');
INSERT INTO `migrations` VALUES ('16', '2017_08_08_063014_create_jobInfos_table', '1');
INSERT INTO `migrations` VALUES ('17', '2017_08_08_080646_create_expoItems_table', '1');

-- ----------------------------
-- Table structure for `professionals`
-- ----------------------------
DROP TABLE IF EXISTS `professionals`;
CREATE TABLE `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `professionals_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of professionals
-- ----------------------------

-- ----------------------------
-- Table structure for `skills`
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of skills
-- ----------------------------

-- ----------------------------
-- Table structure for `telephones`
-- ----------------------------
DROP TABLE IF EXISTS `telephones`;
CREATE TABLE `telephones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `telephones_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of telephones
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `givenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephonevis` tinyint(4) NOT NULL DEFAULT '0',
  `emailvis` tinyint(4) NOT NULL DEFAULT '0',
  `wechatvis` tinyint(4) NOT NULL DEFAULT '0',
  `facebooksvis` tinyint(4) NOT NULL DEFAULT '0',
  `educationvis` tinyint(4) NOT NULL DEFAULT '0',
  `professionalvis` tinyint(4) NOT NULL DEFAULT '0',
  `skillvis` tinyint(4) NOT NULL DEFAULT '0',
  `access` tinyint(4) NOT NULL DEFAULT '0',
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for `watches`
-- ----------------------------
DROP TABLE IF EXISTS `watches`;
CREATE TABLE `watches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `watch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `watches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of watches
-- ----------------------------
