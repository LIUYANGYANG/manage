﻿# Host: 127.0.0.1  (Version: 5.5.40)
# Date: 2017-12-04 07:45:33
# Generator: MySQL-Front 5.3  (Build 4.120)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "banner"
#

DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '图片地址',
  `cate_id` int(10) NOT NULL DEFAULT '0' COMMENT '位置',
  `desc` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT '1' COMMENT '是否显示',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "banner"
#

/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'/uploads/20171105//bd991ec08e396cb0f2a61115b17e6d08.png',2,'http://yangakw.cn/','http://yangakw.cn/',1,NULL,NULL),(6,'/uploads/20171105/e7eb29ec4061af0497b1bad3dca22ad5.gif',7,'啊飒飒','啊飒飒',1,NULL,NULL),(7,'/uploads/20171118/5a2698369e0b5784068ec7f633bb4939.jpg',8,'adfdfdffd','http://baidu.com',1,NULL,NULL);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

#
# Structure for table "banner_cate"
#

DROP TABLE IF EXISTS `banner_cate`;
CREATE TABLE `banner_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "banner_cate"
#

/*!40000 ALTER TABLE `banner_cate` DISABLE KEYS */;
INSERT INTO `banner_cate` VALUES (7,'banner123','banner123',NULL,NULL),(8,'index_lunbo','主页轮播',NULL,NULL);
/*!40000 ALTER TABLE `banner_cate` ENABLE KEYS */;

#
# Structure for table "beenwatches"
#

DROP TABLE IF EXISTS `beenwatches`;
CREATE TABLE `beenwatches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beenwatch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `beenwatches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "beenwatches"
#

/*!40000 ALTER TABLE `beenwatches` DISABLE KEYS */;
/*!40000 ALTER TABLE `beenwatches` ENABLE KEYS */;

#
# Structure for table "educations"
#

DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `education` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `educations_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "educations"
#

/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

#
# Structure for table "emails"
#

DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emails_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "emails"
#

/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;

#
# Structure for table "expocomments"
#

DROP TABLE IF EXISTS `expocomments`;
CREATE TABLE `expocomments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expocomments_user_id_foreign` (`user_id`),
  KEY `expocomments_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "expocomments"
#

/*!40000 ALTER TABLE `expocomments` DISABLE KEYS */;
/*!40000 ALTER TABLE `expocomments` ENABLE KEYS */;

#
# Structure for table "expodetails"
#

DROP TABLE IF EXISTS `expodetails`;
CREATE TABLE `expodetails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expodetails_expo_id_foreign` (`expo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "expodetails"
#

/*!40000 ALTER TABLE `expodetails` DISABLE KEYS */;
INSERT INTO `expodetails` VALUES (1,'椅子板','a','/uploads/20171110/e8fff1784b8af9ff410065c9ee5e178a.jpg',1),(2,'啤酒瓶','1','/uploads/20171111/fb8465700a17e9a07daae37af13825fc.jpg',2),(3,'快快快','1','/uploads/20171112/d9415e38f3c0994b727eeab12a80a3fe.png',1),(4,'232332323','23233232','/uploads/20171118/e62ee7e1a1e3f8317ab328fdcd60d540.jpg',3);
/*!40000 ALTER TABLE `expodetails` ENABLE KEYS */;

#
# Structure for table "expoitems"
#

DROP TABLE IF EXISTS `expoitems`;
CREATE TABLE `expoitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expodetail_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expoitems_expodetail_id_foreign` (`expodetail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "expoitems"
#

/*!40000 ALTER TABLE `expoitems` DISABLE KEYS */;
INSERT INTO `expoitems` VALUES (1,'椅子板正面','11','/uploads/20171110/772d97f29bddacc01b2d8c969b179bfb.jpg',1),(3,'小啤酒瓶','111','/uploads/20171111/fc0471e8723c47031174f670e9037a8a.jpg',2),(4,'椅子腿','12','/uploads/20171112/5e716529cb19d7a7e9af636941f842b8.jpg',1),(5,'111','21121','/uploads/20171112/66c60acc9dd72c87ec2abdf674f303c4.jpg',2),(6,'qweqwe','qweqew','/uploads/20171118/a1199523c6689150a8e643445b51307f.jpg',4),(7,'qweqwe','qweqwe','/uploads/20171118/bffb720d5cfdc4e58576419daff360b2.jpg',4);
/*!40000 ALTER TABLE `expoitems` ENABLE KEYS */;

#
# Structure for table "expos"
#

DROP TABLE IF EXISTS `expos`;
CREATE TABLE `expos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starttime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost` double(8,2) DEFAULT NULL,
  `taglist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `followcount` int(10) unsigned NOT NULL DEFAULT '0',
  `likecount` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `img_cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图片封面',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expos_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "expos"
#

/*!40000 ALTER TABLE `expos` DISABLE KEYS */;
INSERT INTO `expos` VALUES (1,'椅子','official','2017-11-10 22:49:37','2017-11-10 22:49:39',1.00,'1','rows','1',0,0,0,'/uploads/20171203/7db9aad654d49af8177c06e4f5597743.png',NULL,NULL),(2,'啤酒','personal','2017-10-31 00:16:04','2017-11-28 00:16:05',12.00,'1221221','12','12',0,0,0,'',NULL,NULL),(3,'3123213','official','2017-11-18 20:03:42','2017-11-18 20:03:43',123.00,'123123','cols','12312332',0,0,0,'/uploads/20171203/fe1669c6be82f6d43e678d829cc8ecaf.jpg',NULL,NULL);
/*!40000 ALTER TABLE `expos` ENABLE KEYS */;

#
# Structure for table "languages"
#

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `languages_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "languages"
#

/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "migrations"
#

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_07_23_073250_create_users_table',1),(2,'2017_07_23_073329_create_articles_table',1),(3,'2017_07_23_073447_create_articlecomments_table',1),(4,'2017_07_23_073502_create_articledetails_table',1),(5,'2017_07_23_073526_create_educations_table',1),(6,'2017_07_23_073545_create_emails_table',1),(7,'2017_07_23_073557_create_expos_table',1),(8,'2017_07_23_073614_create_expocomments_table',1),(9,'2017_07_23_073627_create_expodetails_table',1),(10,'2017_07_23_073700_create_languages_table',1),(11,'2017_07_23_073715_create_professionals_table',1),(12,'2017_07_23_073729_create_skills_table',1),(13,'2017_07_23_073744_create_telephones_table',1),(14,'2017_07_23_073804_create_watches_table',1),(15,'2017_07_23_073835_create_watchedes_table',1),(16,'2017_08_08_063014_create_jobInfos_table',1),(17,'2017_08_08_080646_create_expoItems_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

#
# Structure for table "professionals"
#

DROP TABLE IF EXISTS `professionals`;
CREATE TABLE `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `professionals_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "professionals"
#

/*!40000 ALTER TABLE `professionals` DISABLE KEYS */;
/*!40000 ALTER TABLE `professionals` ENABLE KEYS */;

#
# Structure for table "skills"
#

DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skills_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "skills"
#

/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;

#
# Structure for table "telephones"
#

DROP TABLE IF EXISTS `telephones`;
CREATE TABLE `telephones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `telephones_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "telephones"
#

/*!40000 ALTER TABLE `telephones` DISABLE KEYS */;
/*!40000 ALTER TABLE `telephones` ENABLE KEYS */;

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `givenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephonevis` tinyint(4) NOT NULL DEFAULT '0',
  `emailvis` tinyint(4) NOT NULL DEFAULT '0',
  `wechatvis` tinyint(4) NOT NULL DEFAULT '0',
  `facebooksvis` tinyint(4) NOT NULL DEFAULT '0',
  `educationvis` tinyint(4) NOT NULL DEFAULT '0',
  `professionalvis` tinyint(4) NOT NULL DEFAULT '0',
  `skillvis` tinyint(4) NOT NULL DEFAULT '0',
  `access` tinyint(4) NOT NULL DEFAULT '0',
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "users"
#

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'3236','111','111','111','111','111',NULL,NULL,111,0,0,0,0,0,0,110,NULL,NULL,NULL),(2,'111','3049a1f0f1c808cdaa4fbed0e01649b1','111','111','111','111',NULL,NULL,111,0,0,0,0,0,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

#
# Structure for table "watches"
#

DROP TABLE IF EXISTS `watches`;
CREATE TABLE `watches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `watch` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `watches_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "watches"
#

/*!40000 ALTER TABLE `watches` DISABLE KEYS */;
/*!40000 ALTER TABLE `watches` ENABLE KEYS */;
